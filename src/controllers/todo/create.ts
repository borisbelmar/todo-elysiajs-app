import type { CreateTodoDto } from "../../schema/TodoDto"

interface Request {
  body: CreateTodoDto
}

export default ({ body }: Request) => {
  return {
    data: {
      id: "1",
      title: body.title,
      description: body.description
    }
  }
}
