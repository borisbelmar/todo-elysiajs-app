import type { SignUpDto } from "../../schema/AuthDto"

interface Request {
  body: SignUpDto
}

export default ({ body }: Request) => {
  return {
    data: {
      firstName: body.firstName,
      lastName: body.lastName,
      email: body.email,
      password: body.password
    }
  }
}