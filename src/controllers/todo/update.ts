import type { UpdateTodoDto } from "../../schema/TodoDto"

interface Request {
  params: {
    id: string
  }
  body: UpdateTodoDto
}

export default ({ body, params }: Request) => {
  return {
    data: {
      id: params.id,
      title: body.title,
      description: body.description
    }
  }
}