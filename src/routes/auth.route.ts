import { Elysia } from "elysia"
import { signInSchema, signUpSchema } from "../schema/AuthDto"
import { signIn, signUp } from "../controllers/auth"

export default function getAuthRoutes (app: Elysia<'/v1/auth'>) {
  return app
    .guard({
      detail: {
        tags: ['Auth']
      }
    },
    app => (
      app
        .post('/sign-in', signIn, { body: signInSchema })
        .post('/sign-up', signUp, { body: signUpSchema })
    ))
}