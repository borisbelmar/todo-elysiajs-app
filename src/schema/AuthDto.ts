import { t, type Static } from "elysia"

export const signInSchema = t.Object({
  email: t.String({ format: "email" }),
  password: t.String({ minLength: 8 })
})

export type SignInDto = Static<typeof signInSchema>

export const signUpSchema = t.Object({
  email: t.String({ format: "email" }),
  password: t.String({ minLength: 8 }),
  firstName: t.String({ minLength: 3 }),
  lastName: t.String({ minLength: 3 })
})


export type SignUpDto = Static<typeof signUpSchema>
