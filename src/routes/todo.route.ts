import { Context, Elysia } from "elysia"
import { createTodoSchema, updateTodoSchema } from "../schema/TodoDto"
import { createTodo, deleteTodo, getAllTodos, getTodoById, updateTodo } from "../controllers/todo"
import jwt from "@elysiajs/jwt"
import jwtConfig from "../config/jwt"
import bearer from "@elysiajs/bearer"

export default function getTodoRoutes (app: Elysia<'/v1/todos'>) {
  return app
    .use(jwt(jwtConfig))
    .use(bearer())
    .guard({
      beforeHandle(ctx) {
        console.log(ctx.bearer)
      },
      detail: {
        tags: ['Todo'],
        security: [{ Bearer: [] }]
      }
    }, app => 
      app.get("/", getAllTodos)
      .post("/", createTodo, { body: createTodoSchema })
      .get("/:id", getTodoById)
      .put("/:id", updateTodo, { body: updateTodoSchema })
      .delete("/:id", deleteTodo)
    )
}