interface Request {
  params: {
    id: string
  }
}

export default ({ params }: Request) => {
  return {
    data: {
      id: params.id,
      title: "Todo title",
      description: "Todo description"
    }
  }
}