const swaggerConfig = {
  documentation: {
    info: {
      title: 'Todo App',
      description: 'Todo App API Documentation',
      version: '1.0.0'
    },
    tags: [
      { name: 'Todo', description: 'Todos endpoints' },
      { name: 'Auth', description: 'Authentication endpoints' }
    ],
    components: {
      securitySchemes: {
        Bearer: {
          type: 'http' as const, 
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    }
  },
  path: '/docs'
}

export default swaggerConfig
