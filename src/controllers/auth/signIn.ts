import type { SignInDto } from "../../schema/AuthDto"

interface Request {
  body: SignInDto
}

export default ({ body }: Request) => {
  return {
    data: {
      email: body.email,
      password: body.password
    }
  }
}