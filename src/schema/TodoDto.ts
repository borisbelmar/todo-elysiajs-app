import { t, type Static } from "elysia"

export const createTodoSchema = t.Object({
  title: t.String({ minLength: 3 }),
  description: t.Optional(t.String()),
})

export type CreateTodoDto = Static<typeof createTodoSchema>

export const updateTodoSchema = t.Object({
  title: t.Optional(t.String({ minLength: 3 })),
  description: t.Optional(t.String()),
})

export type UpdateTodoDto = Static<typeof updateTodoSchema>
