import Elysia from "elysia"
import getTodoRoutes from "./todo.route"
import getAuthRoutes from "./auth.route"

export default function getRoutes (app: Elysia<'/v1'>) {
  return app
    .group('/todos', getTodoRoutes)
    .group('/auth', getAuthRoutes)
}