import { Elysia } from "elysia";
import { swagger } from '@elysiajs/swagger'
import getRoutes from "./routes";
import swaggerConfig from "./config/swagger";

const app = new Elysia()
  .use(swagger(swaggerConfig))
  .group('/v1', getRoutes)
  .listen(3000)


console.log(
  `🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`
);
